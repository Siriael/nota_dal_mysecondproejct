local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number
return function( listOfUnits,x,z,radius)
	
	for i=1, #listOfUnits do
		local transporterID = listOfUnits[i]
		Spring.GiveOrderToUnit(
    	transporterID, 
    	CMD.LOAD_UNITS, 
    	{ x,0,z,radius},
    	{"shift"}
		)
	end
end